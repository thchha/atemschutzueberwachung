# Atemschutzüberwachung

Eine Android Applikation zur Unterstützung der Atemschutzüberwachung bei Einsatz von Atemschutz mit Isoliergeräten.
Es handelt sich um __Design__ einer Software und ist entsprechend nicht für den realen Einsatz geeignet.
Hauptgrund dafür ist, dass weder Verhalten noch Logik durch Tests verifiziert wurden.
Die Anzeige mancher Informationen ist ebenfalls nicht optimal.

## Disclaimer

Die Applikation wurde für Android-Tablets mit einer Bildschirmdiagonalen > 7" erstellt.
Als Testgeräte wurden verwendet:
**Amazon Kindle Fire HD** "Amazon Tate" mit CyanogenMod-Version "11-20141112-SNAPSHOT-M12-tate" (Android 4.4.4)
**Samsung Tab S** "SM-T805" mit LineageOS-Version "14.1-20210305-UNOFFICIAL-chagallte" (Android 7.1.2)

Die Lauffähigkeit auf anderen Geräten ist unbekannt.
Da es sich um ein __Design__ einer Software handelt, ist diese nicht für den realen Einsatz geeignet.

## Verbindung zur Wissenschaftlichen Arbeit

Diese App verbindet die wissenschaftlichen Erkenntnisse des Situationsbewusstseins und Usability innerhalb einer Android-Applikation.
Im Hauptteil der Forschungsarbeit wird die Applikation iterativ verbessert.
Die Forschungsarbeit umfasst zudem die UML-Diagramme der Anwendungsdomäne.

Die resultierenden Stände der Applikation zum Ende jeder Iteration sind als Tags hinterlegt.
Zu jedem Tag wurde eine \*.apk-Datei erstellt, die unter `app/release` abgelegt worden:

[iter-1.apk](https://gitlab.com/thchha/atemschutzueberwachung/-/blob/master/app/release/atemschutzueberwachung_iter-1.apk)

[iter-2.apk](https://gitlab.com/thchha/atemschutzueberwachung/-/blob/master/app/release/atemschutzueberwachung_iter-2.apk)

[iter-3.apk](https://gitlab.com/thchha/atemschutzueberwachung/-/blob/master/app/release/atemschutzueberwachung_iter-3.apk)

[iter-4.apk](https://gitlab.com/thchha/atemschutzueberwachung/-/blob/master/app/release/atemschutzueberwachung_iter-4.apk)


## Lizenz

GNU GENERAL PUBLIC LICENSE, Version 2 (oder später)

