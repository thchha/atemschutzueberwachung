/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import de.uniba.wiai.iss.atemschutzueberwachung.domain.Anwendungscontroller;

import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.IA_CONNECT_SERVICE;
import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.NOTIFICATION_CHANNEL_ID;
import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.NOTIFICATION_ID;
import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.RC_OPEN_APPLICATION;

public class OperationService extends Service {

    private final Anwendungscontroller controller = new Anwendungscontroller();

    private NotificationCompat.Builder notificationBuilder;

    @Override
    public void onCreate() {
        super.onCreate();
        final Intent intent = new Intent( this, MainActivity.class )
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .setAction(IA_CONNECT_SERVICE);
        final PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                RC_OPEN_APPLICATION,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder =
                new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                        .setContentTitle("ATEMSCHUTZÜBERWACHUNG")
                        .setShowWhen(true)
                        .setOnlyAlertOnce(true)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pendingIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller.onStop();
        Atemschutz.isForegroundServiceRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        startForeground(NOTIFICATION_ID, notificationBuilder.build());

        return START_NOT_STICKY;
    }

    public Anwendungscontroller getController() {
        return controller;
    }

    public void start() {
        Atemschutz.isForegroundServiceRunning = true;
        controller.onStart();
    }

    private final OperationServiceBinder binder = new OperationServiceBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class OperationServiceBinder extends Binder {
        public OperationService getOperationService() {
            return OperationService.this;
        }
    }
}
