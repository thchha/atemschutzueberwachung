/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.widget.einsatz;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Truppenstatus;

public class TruppenstatusLegendeView extends LinearLayout {
    public TruppenstatusLegendeView(Context context, Truppenstatus s) {
        super(context);

        View.inflate(context, R.layout.view_status_truppe, this);

        ImageView icon = findViewById(R.id.view_status_icon);
        TextView desc = findViewById(R.id.view_status_desc);

        int res = 0;
        String text = "";
        switch (s) {
            case NICHT_EINSATZBEREIT:
                text = "Nicht Einsatzbereit";
                res = R.drawable.ic_not_ready;
            case EINSATZBEREIT:
                text = "Einsatzbereit";
                res = R.drawable.ic_ready;
                break;
            case AUSGERÜCKT:
                text = "Im Einsatz";
                res = R.drawable.ic_action;
                break;
            case ERFOLGREICH:
                text = "Im Einsatz, erfolgreich";
                res = R.drawable.ic_action;
                break;
            case RÜCKKEHREND:
                text = "Auf Rückweg";
                res = R.drawable.ic_rueckzug;
                break;
            case RUHEPAUSE:
                text = "In Ruhepause";
                res = R.drawable.ic_break;
                break;
            case RETTUNGSEINSATZ:
                text = "IM RETTUNGSEINSATZ";
                res = R.drawable.ic_action;
                break;
        }

        icon.setImageResource(res);
        desc.setText(text);
    }
}
