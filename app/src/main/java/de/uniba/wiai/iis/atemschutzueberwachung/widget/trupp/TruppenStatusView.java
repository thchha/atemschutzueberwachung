/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.widget.trupp;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;

public class TruppenStatusView extends ConstraintLayout implements TruppDetailViewStatus.Consumer {

    private final TextView stateTextView;
    private final TextView summaryTextView;

    public TruppenStatusView(@NonNull Context context) {
        this(context, null);
    }

    public TruppenStatusView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TruppenStatusView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View.inflate(context, R.layout.view_trupp_status, this);
        stateTextView = findViewById(R.id.view_state_title);
        summaryTextView = findViewById(R.id.view_state_summary);

        setInternalState(TruppDetailViewStatus.ERSTELLT);
    }

    @Override
    public void setInternalState(TruppDetailViewStatus state) {
        stateTextView.setText(state.status);
        summaryTextView.setText(state.summary);
    }
}
