/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.einsatz;

import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.InteractionRequest;

public class AtemschutzAktionenViewHolder extends RecyclerView.ViewHolder {

    private static final int urgent = R.drawable.item_urgent_background;
    private static final int normal = R.drawable.item_normal_background;
    private final TextView textView_troup;
    private final TextView textView_action;
    private final TextView textView_remaining_time;
    private final ImageView imageView_action;

    public AtemschutzAktionenViewHolder(@NonNull View itemView) {
        super(itemView);
        textView_troup = itemView.findViewById(R.id.item_action_status_troup);
        textView_action = itemView.findViewById(R.id.item_action_name);
        textView_remaining_time = itemView.findViewById(R.id.item_action_status_remain);
        imageView_action = itemView.findViewById(R.id.item_action_status_icon);
    }

    public void bind(Pair<Atemschutzeinsatz, InteractionRequest> pair) {
        Atemschutzeinsatz e = pair.first;
        InteractionRequest ev = pair.second;

        itemView.setBackgroundResource(ev.isDone() ? urgent : normal);

        textView_troup.setText(e.getTroup().getTroupDetails().getTeamDescription());

        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
        Date d = new Date(ev.getEstimatedTimeStamp());
        long restTime = System.currentTimeMillis() - d.getTime();

        textView_remaining_time.setText(String.format("%s\n%s min", sdfTime.format(d), TimeUnit.MILLISECONDS.toMinutes(restTime)));

        textView_action.setText(ev.getText());

        @DrawableRes int drawable = 0;
        switch (ev){
            default:
                throw new IllegalStateException("Invalid Event to display to the user.");
            case LUFTSTANDSABFRAGE:
                drawable = R.drawable.ic_air;
                break;
            case RÜCKZUGSAUFFORDERUNG:
                drawable = R.drawable.ic_rueckzug;
                break;
            case RÜCKKEHRERWARTUNG:
                drawable = R.drawable.ic_break;
                break;
            case RUHEZEITENENDE:
                drawable = R.drawable.ic_ready;
                break;
        }
        imageView_action.setImageResource(drawable);

    }
}
