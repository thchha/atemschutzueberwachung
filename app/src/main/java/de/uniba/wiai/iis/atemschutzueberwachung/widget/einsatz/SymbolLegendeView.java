/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.widget.einsatz;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.LinkedList;
import java.util.List;

import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Truppenstatus;

public class SymbolLegendeView extends HorizontalScrollView {

    private final List<Truppenstatus> data = new LinkedList<>();
    private LinearLayout container;

    public SymbolLegendeView(Context context) {
        super(context);
    }

    public SymbolLegendeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SymbolLegendeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void showParticularData(List<Atemschutzeinsatz> data) {
        this.data.clear();
        for (Atemschutzeinsatz e : data) {
            Truppenstatus state = e.getState();
            if (this.data.contains(state)) continue;
            this.data.add(state);
        }

        if (container == null) {
            container = new LinearLayout(getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            container.setLayoutParams(params);
            container.setOrientation(LinearLayout.HORIZONTAL);
            addView(container);
        } else {
            container.removeAllViews();
        }
        for (Truppenstatus s : this.data) {
            container.addView(new TruppenstatusLegendeView(getContext(), s));
        }
        
        if (data.size() == 0) {
            TextView tv = new TextView(getContext());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(24, 4, 12, 4);
            tv.setLayoutParams(lp);
            tv.setText("Keine Truppen eingesetzt.");
            container.addView(tv);
        }


        requestLayout();
    }
}
