/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.widget.trupp;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Luftstand;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Zeitpunkt;

public class TruppLuftstandZählerView extends LinearLayout {

    private final ImageButton removeLastBtn;
    private final TextView checkpointNo;
    private final TextView current;
    private final TextView minimal;

    public TruppLuftstandZählerView(Context context) {
        this(context, null);
    }

    public TruppLuftstandZählerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TruppLuftstandZählerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View.inflate(context, R.layout.view_trupp_luftstand, this);

        removeLastBtn = findViewById(R.id.view_luftstand_remove);
        checkpointNo = findViewById(R.id.view_luftstand_ctn);
        current = findViewById(R.id.view_luftstand_current_air);
        minimal = findViewById(R.id.view_luftstand_return_air);
    }

    public void setData(Atemschutzeinsatz attack) {
        List<Luftstand> checks = new LinkedList<>();
        for (Zeitpunkt p : attack.getAchievements()) {
            if (p instanceof Luftstand) {
                checks.add((Luftstand) p);
            }
        }

        if (checks.size() == 0) {
            removeLastBtn.setVisibility(GONE);
            removeLastBtn.setEnabled(false);
            checkpointNo.setText("----");
            minimal.setText("");
            current.setText("");
        } else {
            removeLastBtn.setVisibility(VISIBLE);
            removeLastBtn.setEnabled(true);
            Luftstand first = checks.get(0);
            Luftstand last = checks.get(checks.size() -1);
            double min = first.getAvailableAir() * 0.66;
            minimal.setText(String.format("%.1f bar", min));
            current.setText(String.format("%.1f bar", last.getAvailableAir()));
            if (checks.size() == 1) {
                checkpointNo.setText("initial");
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                checkpointNo.setText(sdf.format(new Date(last.timeStamp)));
            }
        }
    }
}
