/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.einsatz;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutztrupp;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Luftstand;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Truppenstatus;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Zeitpunkt;

public class TruppenViewHolder extends RecyclerView.ViewHolder {

    private TextView troup_desc;
    private TextView troup_funk;
    private TextView troup_state;
    private ImageView troup_state_icon;
    private ImageView troup_rescue;
    private TextView troup_equipment;
    private TextView troup_activity;
    private TextView troup_air;

    public TruppenViewHolder(@NonNull View itemView) {
        super(itemView);
        troup_desc = itemView.findViewById(R.id.item_atemschutz_troup_desc);
        troup_funk = itemView.findViewById(R.id.item_atemschutz_troup_funk);
        troup_state_icon = itemView.findViewById(R.id.item_atemschutz_troup_status_icon);
        troup_rescue = itemView.findViewById(R.id.item_atemschutz_troup_rescue);
        troup_state = itemView.findViewById(R.id.item_atemschutz_troup_state);
        troup_equipment = itemView.findViewById(R.id.item_atemschutz_troup_equipment);
        troup_activity = itemView.findViewById(R.id.item_atemschutz_troup_activity);
        troup_air = itemView.findViewById(R.id.item_atemschutz_troup_air);
    }

    public void bind(Atemschutzeinsatz attack) {
        Atemschutztrupp troup = attack.getTroup();
        Truppenstatus state = attack.getState();
        String equipment = attack.getRespiratorySystem();
        String activity = troup.getActivity();
        String stateMsg = "";
        Luftstand air = null;
        for (Zeitpunkt z : attack.getAchievements()){
            if (z instanceof Luftstand) {
                air = (Luftstand) z;
            }
        }
        @DrawableRes int icon = 0;
        switch (state) {
            case NICHT_EINSATZBEREIT:
                stateMsg = "Nicht Einsatzbereit";
                icon = R.drawable.ic_not_ready;
            case EINSATZBEREIT:
                stateMsg = "Einsatzbereit";
                icon = R.drawable.ic_ready;
                break;
            case AUSGERÜCKT:
                stateMsg = "Im Einsatz";
                icon = R.drawable.ic_action;
                break;
            case ERFOLGREICH:
                stateMsg = "Im Einsatz, erfolgreich";
                icon = R.drawable.ic_action;
                break;
            case RÜCKKEHREND:
                stateMsg = "Auf Rückweg";
                icon = R.drawable.ic_rueckzug;
                break;
            case RUHEPAUSE:
                stateMsg = "In Ruhepause";
                icon = R.drawable.ic_break;
                break;
            case RETTUNGSEINSATZ:
                stateMsg = "IM RETTUNGSEINSATZ";
                icon = R.drawable.ic_action;
                break;
        }

        if (activity.contains("Sicherheitstrupp")) {
            troup_rescue.setVisibility(View.VISIBLE);
        }

        troup_desc.setText(troup.getTroupDetails().getTeamDescription());
        troup_funk.setText(troup.getTroupDetails().getRadioChannel());
        troup_state.setText(stateMsg);
        troup_state_icon.setImageResource(icon);
        troup_equipment.setText(equipment);
        troup_activity.setText(activity);
        if (null != air)
            troup_air.setText(String.format("Luftstand:\n%.1f bar", air.getAvailableAir()));
    }
}
