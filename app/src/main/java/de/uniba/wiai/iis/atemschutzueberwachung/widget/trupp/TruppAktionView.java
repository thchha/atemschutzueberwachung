/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.widget.trupp;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Ausrückung;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Rückkehr;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Rückzugsantritt;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Truppenstatus;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Zeitpunkt;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Zielerfüllung;

public class TruppAktionView extends ConstraintLayout {

    private final ImageButton addAusrueckung;
    private final TextView tsAusrueckung;
    private final ImageButton remAusrueckung;

    private final ImageButton addEinsatzziel;
    private final TextView tsEinsatzziel;
    private final ImageButton remEinsatzziel;

    private final TextView calcTimeRueckzugAt;
    private final TextView calcMinRueckzugIn;

    private final ImageButton addRueckzug;
    private final TextView tsRueckzug;
    private final ImageButton remRueckzug;

    private final ImageButton addRueckkehr;
    private final TextView tsRueckkehr;
    private final ImageButton remRueckkehr;

    private final SimpleDateFormat timeStamp = new SimpleDateFormat("HH:mm");

    private Callback callback;

    private static final long ONE_MINUTE = 60000L;

    public TruppAktionView(Context context) {
        this(context, null);
    }

    public TruppAktionView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TruppAktionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View.inflate(context, R.layout.view_trupp_aktion, this);

        addAusrueckung = findViewById(R.id.view_aktion_ausrueckung_add);
        tsAusrueckung = findViewById(R.id.view_aktion_ausrueckung_timestamp);
        remAusrueckung = findViewById(R.id.view_aktion_ausrueckung_remove);
        addAusrueckung.setOnClickListener(v -> {
            v.setVisibility(View.GONE);
            Date d = new Date();
            callback.setAusrückung(d);
            tsAusrueckung.setText(timeStamp.format(d));
            ( (LinearLayout) tsAusrueckung.getParent()).setVisibility(View.VISIBLE);
        });
        remAusrueckung.setOnClickListener(v -> {
            ( (LinearLayout) tsAusrueckung.getParent()).setVisibility(View.GONE);
            addAusrueckung.setVisibility(View.VISIBLE);
            callback.setAusrückung(null);
        });

        addEinsatzziel = findViewById(R.id.view_aktion_einsatzziel_add);
        tsEinsatzziel = findViewById(R.id.view_aktion_einsatzziel_timestamp);
        remEinsatzziel = findViewById(R.id.view_aktion_einsatzziel_remove);
        addEinsatzziel.setOnClickListener(v -> {
            v.setVisibility(View.GONE);
            Date d = new Date();
            callback.setSuccess(d);
            tsEinsatzziel.setText(timeStamp.format(d));
            ( (LinearLayout) tsEinsatzziel.getParent()).setVisibility(View.VISIBLE);
        });
        remEinsatzziel.setOnClickListener(v -> {
            ( (LinearLayout) tsEinsatzziel.getParent()).setVisibility(View.GONE);
            addEinsatzziel.setVisibility(View.VISIBLE);
            callback.setSuccess(null);
        });

        calcTimeRueckzugAt = findViewById(R.id.view_aktion_rueckkehr_zeit);
        calcMinRueckzugIn = findViewById(R.id.view_aktion_rueckkehr_min);

        addRueckzug = findViewById(R.id.view_aktion_rueckzug_add);
        tsRueckzug = findViewById(R.id.view_aktion_rueckzug_timestamp);
        remRueckzug = findViewById(R.id.view_aktion_rueckzug_remove);
        addRueckzug.setOnClickListener(v -> {
            v.setVisibility(View.GONE);
            Date d = new Date();
            callback.setRückzug(d);
            tsRueckzug.setText(timeStamp.format(d));
            ( (LinearLayout) tsRueckzug.getParent()).setVisibility(View.VISIBLE);
        });
        remRueckzug.setOnClickListener(v -> {
            ( (LinearLayout) tsRueckzug.getParent()).setVisibility(View.GONE);
            addRueckzug.setVisibility(View.VISIBLE);
            callback.setRückzug(null);
        });

        addRueckkehr = findViewById(R.id.view_aktion_rueckkehr_add);
        tsRueckkehr = findViewById(R.id.view_aktion_rueckkehr_timestamp);
        remRueckkehr = findViewById(R.id.view_aktion_rueckkehr_remove);
        addRueckkehr.setOnClickListener(v -> {
            v.setVisibility(View.GONE);
            // TODO: Inform callback about created Date-data
            Date d = new Date();
            callback.setZurück(d);
            tsRueckkehr.setText(timeStamp.format(d));
            ( (LinearLayout) tsRueckkehr.getParent()).setVisibility(View.VISIBLE);
        });
        remRueckkehr.setOnClickListener(v -> {
            ( (LinearLayout) tsRueckkehr.getParent()).setVisibility(View.GONE);
            addRueckkehr.setVisibility(View.VISIBLE);
            callback.setZurück(null);
        });

    }

    public void setData(Atemschutzeinsatz attack) {
        for (Zeitpunkt p : attack.getAchievements()) {
            if (p instanceof Ausrückung) {
                addAusrueckung.setVisibility(View.GONE);
                Date d = new Date(p.timeStamp);
                tsAusrueckung.setText(timeStamp.format(d));
                ( (LinearLayout) tsAusrueckung.getParent()).setVisibility(View.VISIBLE);

            }
            if (p instanceof Zielerfüllung) {

                addEinsatzziel.setVisibility(View.GONE);
                Date d = new Date(p.timeStamp);
                tsEinsatzziel.setText(timeStamp.format(d));
                ( (LinearLayout) tsEinsatzziel.getParent()).setVisibility(View.VISIBLE);
            }
            if (p instanceof Rückzugsantritt) {

                addRueckzug.setVisibility(View.GONE);
                Date d = new Date(p.timeStamp);
                tsRueckzug.setText(timeStamp.format(d));
                ( (LinearLayout) tsRueckzug.getParent()).setVisibility(View.VISIBLE);
            }
            if (p instanceof Rückkehr) {

                addRueckkehr.setVisibility(View.GONE);
                Date d = new Date(p.timeStamp);
                tsRueckkehr.setText(timeStamp.format(d));
                ( (LinearLayout) tsRueckkehr.getParent()).setVisibility(View.VISIBLE);
            }
        }

        long maxTime = attack.getRemainingAttackTime();
        int times = (int) (maxTime / ONE_MINUTE);
        long floored = maxTime * times;

        if (maxTime == 0L) {
            calcTimeRueckzugAt.setText("--");
            calcMinRueckzugIn.setText("--");
        } else {
            String calTimeStamp = timeStamp.format(new Date(System.currentTimeMillis() + floored));
            String calMin = String.format("%02d Min.",
                    TimeUnit.MILLISECONDS.toMinutes(floored));
            calcTimeRueckzugAt.setText(calTimeStamp);
            calcMinRueckzugIn.setText(calMin);
        }

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        callback = null;
    }

    public void attachCallbacks(Callback callback) {
       this.callback = callback;
    }

    /**
     * @param attack
     * @param state
     */
    public void disableInvalidActions(Truppenstatus state) {
        // init: disable everything.
        addAusrueckung.setEnabled(false);
        addRueckzug.setEnabled(false);
        addRueckkehr.setEnabled(false);
        addEinsatzziel.setEnabled(false);

        switch (state) { // fallthrough
            case RETTUNGSEINSATZ:
            case RÜCKKEHREND:
                addRueckkehr.setEnabled(true);
            case ERFOLGREICH:
            case AUSGERÜCKT:
                addEinsatzziel.setEnabled(true);
                addRueckzug.setEnabled(true);
            case EINSATZBEREIT:
                addAusrueckung.setEnabled(true);
            case RUHEPAUSE:
            case NICHT_EINSATZBEREIT:
        }
    }

    public void detachCallbacks() {
        callback = null;
    }

    public interface Callback {

        void setAusrückung(@Nullable Date d);
        void setSuccess(@Nullable Date d);
        void setRückzug(@Nullable Date d);
        void setZurück(@Nullable Date d);
    }
}
