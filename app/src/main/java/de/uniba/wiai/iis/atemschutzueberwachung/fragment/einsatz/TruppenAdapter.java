/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.einsatz;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.core.util.Supplier;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iis.atemschutzueberwachung.widget.einsatz.SymbolLegendeView;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Ausrückung;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.IAnwendungsdatenObserver;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Rückkehr;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Tätigkeit;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Zeitpunkt;

public class TruppenAdapter extends RecyclerView.Adapter<TruppenViewHolder> implements IAnwendungsdatenObserver {

    /* View which rely on the data in this adapter.
    *  WeakReferences, since the adapter can leak a reference when the
    *  orientation changes.
    *  Final, so that creation is forced through the constructor. */
    private final WeakReference<SymbolLegendeView> legendeView;
    private final WeakReference<TextView> ctn_truppen_gesamt;
    private final WeakReference<TextView> ctn_truppen_ausgerückt;
    private final WeakReference<TextView> ctn_truppen_sicherheitstrupps;
    private final Consumer<Atemschutzeinsatz> callback;

    private List<Atemschutzeinsatz> data = new ArrayList<>();

    public TruppenAdapter(SymbolLegendeView legendeView, TextView ctn_truppen_gesamt, TextView ctn_truppen_ausgerückt, TextView ctn_truppen_sicherheitstrupps, Consumer<Atemschutzeinsatz> callback) {
        super();
        this.legendeView = new WeakReference<>(legendeView);
        this.ctn_truppen_gesamt = new WeakReference<>(ctn_truppen_gesamt);
        this.ctn_truppen_ausgerückt = new WeakReference<>(ctn_truppen_ausgerückt);
        this.ctn_truppen_sicherheitstrupps = new WeakReference<>(ctn_truppen_sicherheitstrupps);
        this.callback = callback;
    }

    @NonNull
    @Override
    public TruppenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layoutRes = R.layout.item_atemschutz_trupp;
        View content = LayoutInflater.from(parent.getContext())
                .inflate(layoutRes, parent, false);
        return new TruppenViewHolder(content);
    }

    @Override
    public void onBindViewHolder(@NonNull TruppenViewHolder holder, int position) {
        Atemschutzeinsatz t = data.get(position);
        holder.bind(t);
        holder.itemView.setOnClickListener(v -> callback.accept(t));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onDataUpdated(List<Atemschutzeinsatz> newdata) {
        this.data.clear();
        int inAction = 0;
        int rescue = 0;
        for (Atemschutzeinsatz e : newdata) {
            boolean ignore = false;
            if (e.getTroup().getActivity().toLowerCase().contains(Tätigkeit.SICHERHEITSTRUPP.name().toLowerCase()))
                rescue++;
            for (Zeitpunkt p : e.getAchievements()) {
                if (p instanceof Ausrückung) {
                    inAction++;
                }
               if (p instanceof Rückkehr) {
                   if (System.currentTimeMillis() > p.timeStamp + (1000 * 60 * 30)) {
                       ignore = true;
                   }
               }
            }

            if (!ignore) data.add(e);
        }
        // WeakReference<SymbolLegendeView> legendeView;
        SymbolLegendeView legende = legendeView.get();
        legende.showParticularData(data);

        // TODO: usability, logos.
        TextView gesamt = ctn_truppen_gesamt.get();
        if (gesamt != null)
            gesamt.setText(String.format("%d", getItemCount()));

        TextView tvInAction = ctn_truppen_ausgerückt.get();
        if (tvInAction != null)
            tvInAction.setText(String.format("%d", inAction));

        TextView tvResuce = ctn_truppen_sicherheitstrupps.get();
        if (tvResuce != null)
            tvResuce.setText(String.format("%d", rescue));

        // Sortierung nach Ausrückungszeitpunkt.
        // Erste Ausrückung oben.

        Comparator<Atemschutzeinsatz> orderAttackTimeStamp = new Comparator<Atemschutzeinsatz>() {
            @Override
            public int compare(Atemschutzeinsatz o1, Atemschutzeinsatz o2) {
                Ausrückung p1, p2;
                p1 = getAttackTimeStamp(o1);
                p2 = getAttackTimeStamp(o2);
                if (null == p1 && null == p2) {
                    return 0;
                }
                if (null == p1) {
                    return -1;
                }

                if (null == p2) {
                    return 1;
                }

                if (p1.timeStamp == p2.timeStamp){
                    return 0;
                } else {
                    if (p1.timeStamp > p2.timeStamp) {
                        return 1;
                    } else return -1;
                }
            }

            private Ausrückung getAttackTimeStamp(Atemschutzeinsatz attack) {
                for (Zeitpunkt p : attack.getAchievements()){
                    if (p instanceof Ausrückung)
                        return (Ausrückung) p;
                }
                return null;
            }
        };

        Collections.sort(data, orderAttackTimeStamp);

        notifyDataSetChanged();
    }

}
