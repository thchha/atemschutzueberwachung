/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.widget.trupp;

/**
 * A internal enum to control this view.
 * It is not intended to represent the state of the current object to represent.
 * This should be mapped separately.
 */
public enum TruppDetailViewStatus {
    RETTUNG("IM RETTUNGSEINSATZ", "Sicherheitstrupp ist ausgerückt. Bedarf für neuen Sicherheitstrupp bestimmen!"),
    ERSTELLT("NOCH NICHT AUSGERÜCKT", "Vor Angriff initialien Luftstand eintragen."),
    AUSGERÜCKT("AUSGERÜCKT", "Nach Luftstandsabfrage wird Rückkehr berechnet."),
    RÜCKZUG("RÜCKZUG", "Trupp zieht sich zurück. Luftstandsabfragen weiterhin erforderlich!"),
    RÜCKKEHR("RUHEPAUSE", "Trupp muss sich hydrieren und Mindestpause einhalten.");

    final String status;
    final String summary;

    TruppDetailViewStatus(String status, String summary) {
        this.status = status;
        this.summary = summary;
    }

    /**
     * Interface used by views to declare that they want to consume this enum class
     * as state
     */
    public interface Consumer {

        /**
         * Tell this view what to display.
         *
         * Note that all state transition are valid, since
         * this class does not represent application logic.
         *
         * @param state which decides what to display.
         */
        void setInternalState(TruppDetailViewStatus state);
    }
}
