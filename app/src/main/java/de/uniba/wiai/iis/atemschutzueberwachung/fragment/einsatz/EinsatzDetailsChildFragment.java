/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.einsatz;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz;
import de.uniba.wiai.iis.atemschutzueberwachung.MainActivity;
import de.uniba.wiai.iis.atemschutzueberwachung.OperationService;
import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iis.atemschutzueberwachung.fragment.report.AtemschutzReportFragment;
import de.uniba.wiai.iis.atemschutzueberwachung.fragment.trupp.TruppDetailFragment;
import de.uniba.wiai.iis.atemschutzueberwachung.fragment.trupperfassung.TruppenerfassungFragment;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Anwendungscontroller;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Einsatzbericht;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.IAnwendungsdatenObserver;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.IInteractionRequestListener;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.InteractionRequest;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Truppenstatus;

import static android.os.Looper.getMainLooper;
import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.BDLKEY_ATTACK;
import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.BDLKEY_REPORT;

public class EinsatzDetailsChildFragment extends Fragment {

    public static final String TAG = "fragment_EinsatzDetails";

    private Anwendungscontroller controller;

    private boolean timeIsClicking = true;

    private Button beginOrEndBtn;
    private final CharSequence startString = "EINSATZ STARTEN";
    private final CharSequence endString = "REPORT ERSTELLEN\nBEENDEN";

    /**
     * The ForegroundService to monitor the operation.
     * This object is for managing its lifecycle.
     * Monitoring and emmiting events is done by itself - and delegated to the
     * appropriate fragment.
     */
    private OperationService operationService;

    private AtemschutzAktionenAdapter adapter;

    private ValueAnimator blinkingAnimation;
    private ValueAnimator endAnimation;

    private final IAnwendungsdatenObserver automaticStart = data -> {
        for (Atemschutzeinsatz e : data) {
            Truppenstatus s = e.getState();
            switch (s) {
                case NICHT_EINSATZBEREIT:
                case EINSATZBEREIT:
                    break;
                case AUSGERÜCKT:
                case ERFOLGREICH:
                case RÜCKKEHREND:
                case RUHEPAUSE:
                case RETTUNGSEINSATZ:
                    if (!Atemschutz.isForegroundServiceRunning)
                        beginOrEndBtn.callOnClick();
                    break;
            }
        }
    };

    private final IInteractionRequestListener backgroundHighlightListener = new IInteractionRequestListener() {
        @Override
        public void onInteractionRequested(HashMap<Atemschutzeinsatz, List<InteractionRequest>> map) {
            new Handler(getMainLooper()).post(() -> {
                long nowMinusThirty = System.currentTimeMillis() - 30000L;
                if (Atemschutz.lastInteraction >= nowMinusThirty || map.values().size() == 0) {
                    if (blinkingAnimation.isRunning())
                        blinkingAnimation.end();
                    endAnimation.start();
                } else {
                    if (!blinkingAnimation.isRunning())
                        blinkingAnimation.start();
                }
            });
        }
    };

    private ServiceConnection bindCallback = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            operationService = ( (OperationService.OperationServiceBinder) service).getOperationService();
            controller = operationService.getController();
            controller.registerInteractionRequestListener(adapter);
            controller.registerInteractionRequestListener(backgroundHighlightListener);
            controller.registerObserver(automaticStart);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            beginOrEndBtn.setText(startString);
            beginOrEndBtn.setOnClickListener(null);
            controller.unregisterInteractionRequestListener(adapter);
            controller.unregisterInteractionRequestListener(backgroundHighlightListener);
        }
    };

    private Runnable writableTimeUpdate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_einsatz_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ConstraintLayout rootLayout = view.findViewById(R.id.einsatz_detail_root);

        blinkingAnimation = ((MainActivity) getActivity()).getBlinkingAnimation(false);
        endAnimation = ((MainActivity) getActivity()).getBlinkingAnimation(true);
        ValueAnimator.AnimatorUpdateListener animUpdate = animator -> {
            if (rootLayout != null)
                rootLayout.setBackgroundColor((int) animator.getAnimatedValue());
        };
        endAnimation.addUpdateListener(animUpdate);
        blinkingAnimation.addUpdateListener(animUpdate);

        final TextView clock = view.findViewById(R.id.einsatz_detail_uhrzeit);
        final Handler uiHandler = new Handler(getMainLooper());
        writableTimeUpdate = () -> {
            final CharSequence time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
            clock.setText(time);

            if (timeIsClicking) // revisit/recursive
                uiHandler.postDelayed(writableTimeUpdate, 1000L);
        };

        Consumer<Atemschutzeinsatz> navigateDetailFragment = (attack) -> {
            Bundle bdl = new Bundle();
            bdl.putSerializable(BDLKEY_ATTACK, attack);
            TruppDetailFragment truppDetailFragment = new TruppDetailFragment();
            truppDetailFragment.setArguments(bdl);
            FragmentManager fm = getParentFragment().getChildFragmentManager();
            fm.beginTransaction()
                    .add(R.id.einsatz_uebersicht_root, truppDetailFragment, TruppDetailFragment.TAG)
                    .addToBackStack(null)
                    .commit();
        };

        int orientation = view.getContext().getResources()
                .getConfiguration().orientation == 1 ? RecyclerView.VERTICAL : RecyclerView.HORIZONTAL;
        final LinearLayoutManager llm = new LinearLayoutManager(view.getContext(), orientation, false);
        adapter = new AtemschutzAktionenAdapter(navigateDetailFragment);
        final RecyclerView recyclerView = view.findViewById(R.id.einsatz_detail_aktionen_recycler);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(
                view.getContext(),
                orientation == RecyclerView.VERTICAL ? RecyclerView.HORIZONTAL : RecyclerView.VERTICAL));


        final Activity activity = getActivity();
        final Context ctx = activity.getApplicationContext();
        final Intent serviceIntent = new Intent(ctx, OperationService.class);
        beginOrEndBtn = view.findViewById(R.id.einsatz_detail_begin_or_end_btn);
        beginOrEndBtn.setText( Atemschutz.isForegroundServiceRunning ? endString : startString);
        beginOrEndBtn.setOnClickListener(v -> {
            if (Atemschutz.isForegroundServiceRunning) {
                Anwendungscontroller controller = operationService.getController();
                controller.unregisterInteractionRequestListener(adapter);
                Einsatzbericht report = controller.produceReport();
                if (null != operationService)
                    operationService.stopForeground(true);
                ctx.stopService(serviceIntent);

                Bundle bdl = new Bundle();
                bdl.putSerializable(BDLKEY_REPORT, report);
                AtemschutzReportFragment reportFragment = new AtemschutzReportFragment();
                reportFragment.setArguments(bdl);
                getParentFragment()
                        .getChildFragmentManager().beginTransaction()
                        .add(R.id.einsatz_uebersicht_root, reportFragment, AtemschutzReportFragment.TAG)
                        .addToBackStack(null)
                        .commit();
            } else {
                operationService.start();
                beginOrEndBtn.setText(endString);
            }
        });

        view.findViewById(R.id.einsatz_detail_trupperfassung_btn)
                .setOnClickListener(v -> {
                    TruppenerfassungFragment truppenerfassungFragment = new TruppenerfassungFragment();
                    getParentFragment()
                            .getChildFragmentManager().beginTransaction()
                            .add(R.id.einsatz_uebersicht_root, truppenerfassungFragment, TruppenerfassungFragment.TAG)
                            .addToBackStack(null)
                            .commit();
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        final Intent serviceIntent = new Intent(getContext(), OperationService.class);
        getContext().bindService(serviceIntent, bindCallback, Service.BIND_IMPORTANT);
        writableTimeUpdate.run();
    }

    @Override
    public void onPause() {
        super.onPause();
        timeIsClicking = false;
        getContext().unbindService(bindCallback);
    }
}
