/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.einsatz;

import android.os.Handler;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.IInteractionRequestListener;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.InteractionRequest;

import static android.os.Looper.getMainLooper;

public class AtemschutzAktionenAdapter extends RecyclerView.Adapter<AtemschutzAktionenViewHolder> implements IInteractionRequestListener {

    private final List<Pair<Atemschutzeinsatz, InteractionRequest>> data = new LinkedList<>();
    private final Consumer<Atemschutzeinsatz> navigateDetailFragment;

    public AtemschutzAktionenAdapter(Consumer<Atemschutzeinsatz> navigateDetailFragment) {
        super();
        this.navigateDetailFragment = navigateDetailFragment;
    }

    @NonNull
    @Override
    public AtemschutzAktionenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View content = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_atemschutz_ueberwachung_aktion, parent, false);
        return new AtemschutzAktionenViewHolder(content);
    }

    @Override
    public void onBindViewHolder(@NonNull AtemschutzAktionenViewHolder holder, int position) {
        Pair<Atemschutzeinsatz, InteractionRequest> pair = data.get(position);
        holder.bind(pair);
        holder.itemView.setOnClickListener(v -> {
            navigateDetailFragment.accept(pair.first);
            int i = data.indexOf(pair);
            data.remove(pair);
            notifyItemRemoved(i);
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public void onInteractionRequested(HashMap<Atemschutzeinsatz, List<InteractionRequest>> map) {
        new Handler(getMainLooper()).post(() -> {
            data.clear();

            for (Map.Entry<Atemschutzeinsatz, List<InteractionRequest>> e : map.entrySet()) {
                for (InteractionRequest req : e.getValue()) {
                    data.add(new Pair<>(e.getKey(), req));
                }
            }

            Comparator<Pair<Atemschutzeinsatz, InteractionRequest>> urgencyComparator = new Comparator<Pair<Atemschutzeinsatz, InteractionRequest>>() {
                @Override
                public int compare(Pair<Atemschutzeinsatz, InteractionRequest> o1, Pair<Atemschutzeinsatz, InteractionRequest> o2) {
                    if (null == o1 && null == o2) {
                        return 0;
                    } else if (null == o1) {
                        return -1;
                    } else if (null == o2) {
                        return 1;
                    } else {
                        InteractionRequest e1 = o1.second;
                        InteractionRequest e2 = o2.second;

                        if (e1.getEstimatedTimeStamp() == e2.getEstimatedTimeStamp()) {
                            return 0;
                        } else if (e1.getEstimatedTimeStamp() > e2.getEstimatedTimeStamp()) {
                            return 1;
                        } else
                            return -1;
                    }
                }
            };
            Collections.sort(data, urgencyComparator);
            notifyDataSetChanged();
        });
    }
}
