/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.luftstandsabgleich;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.textfield.TextInputLayout;

import java.util.LinkedList;
import java.util.List;

import de.uniba.wiai.iis.atemschutzueberwachung.OperationService;
import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iis.atemschutzueberwachung.fragment.trupp.TruppDetailFragment;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Anwendungscontroller;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.AnwendungslogikException;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Luftstand;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Zeitpunkt;

import static android.content.Context.VIBRATOR_SERVICE;
import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.BDLKEY_ATTACK;

public class LuftstandAbgleichFragment extends Fragment {

    public static final String TAG = "frag_Luftstand";

    private Atemschutzeinsatz attack;
    private Luftstand last;
    private String times;
    private double value;
    private double reserve;

    private Anwendungscontroller controller;

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            controller = ((OperationService.OperationServiceBinder) service).getOperationService().getController();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent serviceIntent = new Intent(getContext(), OperationService.class);
        getContext().bindService(serviceIntent, serviceConnection, Context.BIND_IMPORTANT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_luftstand, container, false);
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);

        if (args == null) throw new IllegalStateException(String.format("%s requires a Atemschutzeinsatz obj", this.getClass().toString()));

        attack = (Atemschutzeinsatz) args.getSerializable(BDLKEY_ATTACK);
        List<Luftstand> timeStamps = new LinkedList<>();
        for ( Zeitpunkt ts : attack.getAchievements()) {
            if (ts instanceof Luftstand) {
                timeStamps.add((Luftstand) ts);
            }
        }
        if (0 == timeStamps.size()) {
           times = "Initialer Luftstand";
           last = null;
           value = 3.0;
           reserve = 0.0;
        } else {
            reserve = timeStamps.get(0).getAvailableAir() * 0.66;
            last = timeStamps.get(timeStamps.size() -1);
            times = String.format("%d. Messung", timeStamps.size());
            value = last.getAvailableAir() - 0.1;

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView reserveTextView = (TextView) view.findViewById(R.id.luftstand_reserve);
        reserveTextView.setText(String.format("erforderliche Reserve\nfür Rückweg: %.1f", reserve));

        int normalTextColor = reserveTextView.getCurrentTextColor();
        int urgentTextColor = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ?
                view.getContext().getResources().getColor(R.color.urgent, getContext().getTheme())
                : view.getContext().getResources().getColor(R.color.urgent);

        Runnable checkShortfall = () -> {
            int colour = 0;
            if (value <= reserve) {
                colour = urgentTextColor;
                onHaptikFeedbackNecessary();
            } else {
                colour = normalTextColor;
            }
            reserveTextView.setTextColor(colour);
        };

        ((TextView) view.findViewById(R.id.luftstand_truppen_bez)).setText(attack.getTroup().getTroupDetails().getTeamDescription());

        String lastAir = null == last ? "Bisher noch kein Luftstand eingetragen" : String.format("Letzter Luftstand war %s", last);
        ((TextView) view.findViewById(R.id.luftstand_zuletzt))
                .setText(lastAir);
        ((TextView) view.findViewById(R.id.luftstand_messpunkt))
                .setText(times);

        final EditText valueDisplay = ((TextInputLayout) view.findViewById(R.id.luftstand_anzeige)).getEditText();

        valueDisplay.setText(String.format("%.1f", value));

        view.findViewById(R.id.luftstand_increase)
                .setOnClickListener( v -> {
                    value += 0.1;
                    checkShortfall.run();
                    valueDisplay.setText(String.format("%.1f", value));
                });

        view.findViewById(R.id.luftstand_decrease)
                .setOnClickListener( v -> {
                    value -= 0.1;
                    checkShortfall.run();
                    if (value < 0.0) {
                        value = 0;
                    }
                    valueDisplay.setText(String.format("%.1f", value));
                });

        view.findViewById(R.id.luftstand_apply_btn)
                .setOnClickListener(v -> {
                    if (null != controller) {
                        try {
                            controller.updateAvailableAir(attack, value);
                        } catch (AnwendungslogikException e) {
                            switch (e.reason) {
                                case LUFTSTANDSMENGE_UNGÜLTIG:
                                    // TODO: usability
                                    return;
                                default:
                                    return;
                            }
                        }
                        FragmentManager fm = getParentFragment().getChildFragmentManager();
                        Fragment frag = fm.findFragmentByTag(TruppDetailFragment.TAG);
                        if (null != frag) {
                            ((TruppDetailFragment) frag).onAirUpdated();
                        }
                        fm.popBackStack();
                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
        getContext().unbindService(serviceConnection);
    }

    private void onHaptikFeedbackNecessary() {
        if (null == getContext()) return;

        long length = 250L;

        Vibrator vibratorManager = (Vibrator) getContext().getSystemService(VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            VibrationEffect vibrationEffect = VibrationEffect.createOneShot(length, VibrationEffect.DEFAULT_AMPLITUDE);
            vibratorManager.vibrate(vibrationEffect);
        } else {
           vibratorManager.vibrate(length);
        }
    }
}
