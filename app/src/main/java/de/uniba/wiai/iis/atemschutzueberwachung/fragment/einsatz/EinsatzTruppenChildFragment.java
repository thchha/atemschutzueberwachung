/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.einsatz;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz;
import de.uniba.wiai.iis.atemschutzueberwachung.MainActivity;
import de.uniba.wiai.iis.atemschutzueberwachung.OperationService;
import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iis.atemschutzueberwachung.fragment.trupp.TruppDetailFragment;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Anwendungscontroller;

import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.BDLKEY_ATTACK;

public class EinsatzTruppenChildFragment extends Fragment {

    public static final String TAG = "fragment_Truppen";

    public TruppenAdapter adapter;

    private Anwendungscontroller controller;

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            controller = ((OperationService.OperationServiceBinder) service).getOperationService().getController();
            controller.registerObserver(adapter);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            controller.unregisterObserver(adapter);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_einsatz_truppen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new TruppenAdapter( /* working on data, not user input: */
                view.findViewById(R.id.truppen_symbol_legende),
                view.findViewById(R.id.truppen_summary_gesamt_ctn),
                view.findViewById(R.id.truppen_summary_ausgerückt_ctn),
                view.findViewById(R.id.truppen_summary_sicherheitstrupp_ctn),
                attack -> {
                    Bundle bdl = new Bundle();
                    bdl.putSerializable(BDLKEY_ATTACK, attack);
                    TruppDetailFragment truppDetailFragment = new TruppDetailFragment();
                    truppDetailFragment.setArguments(bdl);
                    FragmentManager fm = getParentFragment().getChildFragmentManager();
                    fm.beginTransaction()
                            .add(R.id.einsatz_uebersicht_root, truppDetailFragment, TruppDetailFragment.TAG)
                            .addToBackStack(null)
                            .commit();
                }
        );

        final LinearLayoutManager llm = new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false);
        RecyclerView recyclerView = view.findViewById(R.id.truppen_recycler);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        final Intent serviceIntent = new Intent(getContext(), OperationService.class);
        getContext().bindService(serviceIntent, serviceConnection, Context.BIND_IMPORTANT);
    }

    @Override
    public void onStop() {
        super.onStop();
        controller.unregisterObserver(adapter);
        getContext().unbindService(serviceConnection);
    }
}
