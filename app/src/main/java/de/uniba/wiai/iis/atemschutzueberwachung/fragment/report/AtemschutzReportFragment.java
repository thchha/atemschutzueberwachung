/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.report;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.AnwendungslogikException;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Einsatzbericht;

import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.BDLKEY_REPORT;

public class AtemschutzReportFragment extends Fragment {

    public static final String TAG = "frag_report";
    private Einsatzbericht report;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getOnBackPressedDispatcher()
                .addCallback(backpressCallback);
    }

    private final OnBackPressedCallback backpressCallback = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            Toast.makeText(requireContext(), "Prevented backpress. Dialog required.", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);

        if (null == args) throw new IllegalStateException(String.format("%s requires a Einsatzbericht obj", AtemschutzReportFragment.class));

        report = (Einsatzbericht) args.getSerializable(BDLKEY_REPORT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_report, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
        ((TextView) view.findViewById(R.id.report_date)).setText(date.format(new Date()));

        final RecyclerView rv = view.findViewById(R.id.report_recycler_view);
        rv.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(new EinsatzberichtAdapter(report));

        view.findViewById(R.id.report_save_btn)
                .setOnClickListener(v -> {
                    getParentFragment().getChildFragmentManager()
                            .popBackStack();
                    String reasoning, location, personInCharge;

                    personInCharge = ((TextInputLayout) view.findViewById(R.id.report_atemschutz_überachung)).getEditText().getText().toString();
                    location = ((TextInputLayout) view.findViewById(R.id.report_operation_location)).getEditText().getText().toString();
                    reasoning = ((TextInputLayout) view.findViewById(R.id.report_operation_cause)).getEditText().getText().toString();

                    try {
                        String txt = report.getContentAsText(reasoning, location, personInCharge);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("*/*");
                        intent.putExtra(Intent.EXTRA_SUBJECT, String.format("Einsatzbericht Atemschutzüberwachung vom %s", sdf.format(new Date())));
                        intent.putExtra(Intent.EXTRA_TEXT, txt);
                        startActivity(Intent.createChooser(intent, "Send Email"));

                        getActivity().finish();
                        System.exit(0);

                    } catch (AnwendungslogikException e) {
                        switch (e.reason) {
                            case INVALID_ATTACK_STATE:
                                // TODO: usability
                                break;
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        this.backpressCallback.setEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.backpressCallback.setEnabled(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.backpressCallback.remove();
    }
}
