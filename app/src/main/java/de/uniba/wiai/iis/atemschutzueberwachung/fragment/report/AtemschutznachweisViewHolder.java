/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.report;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutznachweis;

public class AtemschutznachweisViewHolder extends RecyclerView.ViewHolder {

    private final TextView tvPerson;
    private final TextView tvDuration;
    private final TextView tvContent;

    public AtemschutznachweisViewHolder(@NonNull View itemView) {
        super(itemView);
        tvPerson = itemView.findViewById(R.id.item_einsatzbericht_name);
        tvDuration = itemView.findViewById(R.id.item_einsatzbericht_gesamt);
        tvContent = itemView.findViewById(R.id.item_einsatzbericht_content);
    }

    public void bind(Atemschutznachweis report) {
        String person = report.getName();
        String duration = report.getCumulativeAttackDuration();
        String content = "";

        for (Atemschutznachweis.Einsatz e : report.getOwnedAttacks()) {
            content += String.format("Dauer der Tätigkeit \"%s\" unter Zuhilfename von \"%s\":\t %s\n", e.getActivity(), e.getEquipment(), e.getDuration());
        }

        tvPerson.setText(person);
        tvDuration.setText(duration);
        tvContent.setText(content);
    }
}
