/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Application;

public class Atemschutz extends Application {

    /**
     * Set by the {@see ForegroundService}.
     * The lifetime of this class is preserved when all activities
     * would be close, as long as the ForegroundService is alive.
     * Otherwise we initialize it again with false when starting the application.
     * The ForegroundService runs on the uiThread, so
     * synchronization is only necessary if we access from a async-Thread.
     */
    public static boolean isForegroundServiceRunning = false;

    public static final String POWER_MANAGER_WAKE_LOCK_REASON = "ATEMSCHUTZÜBERWACHUNG:HANDLUNGSBEDARF";

    public static final String BDLKEY_ATTACK = "atemschuteinsatz";
    public static final String BDLKEY_REPORT = "einsatzbericht";

    public static final int NOTIFICATION_ID = 96052;
    public static final String NOTIFICATION_CHANNEL_ID = "FOREGROUND_NOTIFICATION";
    public static final int RC_OPEN_APPLICATION = 4711;
    public static final String IA_CONNECT_SERVICE = "connect_running_service";

    /**
     * Used to request, when the last interaction has happed
     */
    public static long lastInteraction = 0L;

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
