/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.trupperfassung;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.LinkedList;
import java.util.List;

import de.uniba.wiai.iis.atemschutzueberwachung.MainActivity;
import de.uniba.wiai.iis.atemschutzueberwachung.OperationService;
import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iis.atemschutzueberwachung.fragment.trupp.TruppDetailFragment;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Anwendungscontroller;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.AnwendungslogikException;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutztrupp;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.IAnwendungsdatenObserver;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Stammdaten;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Truppenstatus;

import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.BDLKEY_ATTACK;

public class TruppenerfassungFragment extends Fragment {

    public static final String TAG = "fragment_truppenerfassung";

    private Anwendungscontroller controller;

    private MaterialAutoCompleteTextView autoCompleteTextViewLeader;
    private MaterialAutoCompleteTextView autoCompleteTextViewTroupMember1;
    private MaterialAutoCompleteTextView autoCompleteTextViewTroupMember2;

    private IAnwendungsdatenObserver observer = new IAnwendungsdatenObserver() {
        @Override
        public void onDataUpdated(List<Atemschutzeinsatz> data) {
            List<String> names = new LinkedList<>();
            for (Atemschutzeinsatz e : data) {
                Truppenstatus state = e.getState();
                if (state == Truppenstatus.NICHT_EINSATZBEREIT || state == Truppenstatus.EINSATZBEREIT ) {
                    Stammdaten details = e.getTroup().getTroupDetails();
                    String[] members = new String[] { details.getTeamLeader(), details.getTeamMember_1(), details.getTeamMember_2() };
                    for (String name : members) {
                        if (!names.contains(name))
                            names.add(name);
                    }
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, names);
            autoCompleteTextViewLeader.setAdapter(adapter);
            autoCompleteTextViewTroupMember1.setAdapter(adapter);
            autoCompleteTextViewTroupMember2.setAdapter(adapter);
        }
    };

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            controller = ((OperationService.OperationServiceBinder) service).getOperationService().getController();
            controller.registerObserver(observer);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent serviceIntent = new Intent(getContext(), OperationService.class);
        getContext().bindService(serviceIntent, serviceConnection, Context.BIND_IMPORTANT);
        getActivity().getOnBackPressedDispatcher()
                .addCallback(backPressedCallback);
    }

    private final OnBackPressedCallback backPressedCallback = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            getParentFragment().getChildFragmentManager().popBackStack();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_truppenerfassung, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        autoCompleteTextViewLeader = (MaterialAutoCompleteTextView) view.findViewById(R.id.truppenerfassung_trupp_fuehrer_et);
        autoCompleteTextViewTroupMember1 = (MaterialAutoCompleteTextView) view.findViewById(R.id.truppenerfassung_trupp_mann_1_et);
        autoCompleteTextViewTroupMember2 = (MaterialAutoCompleteTextView) view.findViewById(R.id.truppenerfassung_trupp_mann_2_et);

        view.findViewById(R.id.truppenerfassung_equip_info)
                .setOnClickListener((v) -> {
                    Dialog info = new BottomSheetDialog(view.getContext());
                    info.setContentView(R.layout.dialog_equipment_info);
                    info.setCancelable(true);
                    info.setTitle("Schutzausrüstungabkürzungen");
                    info.show();
                    BottomSheetBehavior.from(info.findViewById(R.id.design_bottom_sheet)).setState(BottomSheetBehavior.STATE_EXPANDED);
                });

        view.findViewById(R.id.truppenerfassung_apply_btn)
                .setOnClickListener(v -> {
                    Stammdaten details = getTroupDetailsFrom(view);
                    // TODO: put in stammdaten (update UML): equipment, activity
                    Atemschutztrupp troup = new Atemschutztrupp(details);
                    int checked_id = ((RadioGroup) view.findViewById(R.id.truppenerfassung_equip_container))
                            .getCheckedRadioButtonId();
                    String system = "";
                    switch (checked_id) {
                        case 1:
                            system = "Chemikalienschutzanzug";
                            break;
                        case 2:
                            system = "Kälteschutzanzug";
                            break;
                        case 3:
                            system = "Pressluftatmer";
                            break;
                        case 4:
                            system = "Wärmeschutzanzug";
                            break;
                        case 5:
                            system = "Sonstiges";
                            break;
                    }
                    StringBuilder builder = new StringBuilder("");
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_1)).isChecked()) {
                        builder.append("Menschenrettung ");
                    }
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_2)).isChecked()) {
                        builder.append("Sicherheitstrupp ");
                    }
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_3)).isChecked()) {
                        builder.append("Erkunden ");
                    }
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_4)).isChecked()) {
                        builder.append("Messen ");
                    }
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_5)).isChecked()) {
                        builder.append("Brandbekämpfung ");
                    }
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_6)).isChecked()) {
                        builder.append("Zubringertätigkeit ");
                    }
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_7)).isChecked()) {
                        builder.append("Sonstige Tätigkeit ");
                    }
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_8)).isChecked()) {
                        builder.append("AS-Strecke / Einsatzübung ");
                    }
                    if (((CheckBox) view.findViewById(R.id.truppenerfassung_task_9)).isChecked()) {
                        builder.append("Unterricht ");
                    }

                    troup.setActivity(builder.toString());

                    if (null == controller)
                        throw new IllegalStateException(String.format("%s was not bound yet", OperationService.class));

                    final String finalSystem = system;
                    View.OnClickListener proceedAction = v1 -> {
                        try {
                            Atemschutzeinsatz attack = null;
                            attack = controller.registerTroup(troup, true);
                            attack.setRespiratorySystem(finalSystem);
                            navigateTruppDetailFragment(attack);
                        } catch (AnwendungslogikException e) {
                            switch (e.reason) {
                                case TEAMMEMBER_ALREADY_INACTION:
                                    Toast.makeText(view.getContext(), "FATAL IMPLEMENTATION ERROR -> prevent recursing now.", Toast.LENGTH_SHORT).show();
                                    getParentFragment().getChildFragmentManager().popBackStack();
                                    break;
                                default:
                                    handleRegistrationException(e);
                                    break;
                            }
                        }
                    };
                    try {
                        Atemschutzeinsatz attack = null;
                        attack = controller.registerTroup(troup, false);
                        attack.setRespiratorySystem(finalSystem);
                        navigateTruppDetailFragment(attack);
                    } catch (AnwendungslogikException e) {
                        switch (e.reason) {
                            case TEAMMEMBER_ALREADY_INACTION:
                                Snackbar hint = Snackbar.make(requireView(), "Es ist bereits ein Atemschutzgeräteträger mit diesen Namen im Einsatz. Fortfahren?", Snackbar.LENGTH_INDEFINITE);
                                hint.setAction("Fortfahren", proceedAction);
                                hint.show();
                                break;
                            default:
                                handleRegistrationException(e);
                                break;
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        this.backPressedCallback.setEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.backPressedCallback.setEnabled(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        controller.unregisterObserver(observer);
        getContext().unbindService(serviceConnection);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.backPressedCallback.remove();
    }

    /**
     * TODO: USABILITY: Verify that the required fields are provided - otherwise prompt appropr.
     *
     * @param view root, which has the references to all ui objects in this frag.
     * @return obj, which holds all the inputs
     */
    private Stammdaten getTroupDetailsFrom(View view) {
        Stammdaten details = new Stammdaten();
        details.setTeamDescription(
                ((TextInputLayout) view.findViewById(R.id.truppenerfassung_trupp_desc)).getEditText().getText().toString()
        );
        details.setTeamLeader(
                ((TextInputLayout) view.findViewById(R.id.truppenerfassung_trupp_fuehrer)).getEditText().getText().toString()
        );
        details.setTeamMember_1(
                ((TextInputLayout) view.findViewById(R.id.truppenerfassung_trupp_mann_1)).getEditText().getText().toString()
        );
        details.setTeamMember_2(
                ((TextInputLayout) view.findViewById(R.id.truppenerfassung_trupp_mann_2)).getEditText().getText().toString()
        );
        details.setRadioChannel(
                ((TextInputLayout) view.findViewById(R.id.truppenerfassung_trupp_funk)).getEditText().getText().toString()
        );
        return details;
    }

    private void navigateTruppDetailFragment(Atemschutzeinsatz attack) {
        String msg = String.format("Atemschutztrupp %s wurde erfolgreich erstellt.", attack.getTroup().getTroupDetails().getTeamDescription());
        ((MainActivity) getActivity()).showUserFeedback(msg);
        Bundle bdl = new Bundle();
        bdl.putSerializable(BDLKEY_ATTACK, attack);

        TruppDetailFragment truppDetailFragment = new TruppDetailFragment();
        truppDetailFragment.setArguments(bdl);
        FragmentManager fm = getParentFragment().getChildFragmentManager();
        fm.popBackStack();
        fm.beginTransaction()
                .add(R.id.einsatz_uebersicht_root, truppDetailFragment, TruppDetailFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    private void handleRegistrationException(AnwendungslogikException e) {
        switch (e.reason) {
            case TEAMMEMBER_ALREADY_INACTION:
                Log.e(this.getClass().getCanonicalName(), "IMPLEMENTATION ERROR, WE SHOULD NOT HAVE CASE 'TEAMMEMBER_ALREADY_INACTION'", e);
                break;
            // TODO: usability
            case LEADER_MISSING:
                break;
            case TEAMMEMBERS_MISSING:
                break;
            case OLD_STATE_APPEARS_INVALID:
                break;
        }
    }
}
