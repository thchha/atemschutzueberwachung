/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.einsatz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iis.atemschutzueberwachung.fragment.trupperfassung.TruppenerfassungFragment;

/**
 * This fragment provides "android:id="@+id/einsatz_uebersicht_root" ", which
 * can be used to add other fragments as children to its layout, resulting in overlaying
 * it and beeing referencable to children.
 */
public class EinsatzUebersichtFragment extends Fragment {

    public static final String TAG = "Frag_EinsatzUebersicht";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_einsatz_uebersicht, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (null == savedInstanceState) {
            Fragment truppenFragment = new EinsatzTruppenChildFragment();
            Fragment einsatzDetailsFragment = new EinsatzDetailsChildFragment();
            getChildFragmentManager().beginTransaction()
                    .add(R.id.einsatz_uebersicht_truppen_fragment_container, truppenFragment, EinsatzTruppenChildFragment.TAG)
                    .add(R.id.einsatz_uebersicht_einsatz_details_container, einsatzDetailsFragment, EinsatzDetailsChildFragment.TAG)
                    .commit();
        }


        view.findViewById(R.id.einsatz_uebersicht_setting_btn)
                .setOnClickListener(v -> {
                    Toast.makeText(view.getContext(), "There are no settings yet.", Toast.LENGTH_SHORT).show();
                });
    }
}
