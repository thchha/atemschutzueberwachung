/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.report;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.AnwendungslogikException;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Einsatzbericht;

public class EinsatzberichtAdapter extends RecyclerView.Adapter<AtemschutznachweisViewHolder> {

    private final Einsatzbericht report;

    public EinsatzberichtAdapter(Einsatzbericht report) {
        this.report = report;
    }

    @NonNull
    @Override
    public AtemschutznachweisViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_einsatzbericht_einsatz, parent, false);
        return new AtemschutznachweisViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AtemschutznachweisViewHolder holder, int position) {
        try {
            holder.bind(report.getTroupReports().get(position));
        } catch (AnwendungslogikException e) {
            switch (e.reason) {
                case INVALID_ATTACK_STATE:
                    // TODO: usability
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        try {
            return report.getTroupReports().size();
        } catch (AnwendungslogikException e) {
            switch (e.reason) {
                case INVALID_ATTACK_STATE:
                    // TODO: usability
                default:
                    return 0;
            }
        }
    }
}
