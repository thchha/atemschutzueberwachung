/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung.fragment.trupp;

import android.animation.ValueAnimator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz;
import de.uniba.wiai.iis.atemschutzueberwachung.MainActivity;
import de.uniba.wiai.iis.atemschutzueberwachung.OperationService;
import de.uniba.wiai.iis.atemschutzueberwachung.R;
import de.uniba.wiai.iis.atemschutzueberwachung.fragment.luftstandsabgleich.LuftstandAbgleichFragment;
import de.uniba.wiai.iis.atemschutzueberwachung.widget.trupp.TruppAktionView;
import de.uniba.wiai.iis.atemschutzueberwachung.widget.trupp.TruppDetailViewStatus;
import de.uniba.wiai.iis.atemschutzueberwachung.widget.trupp.TruppLuftstandZählerView;
import de.uniba.wiai.iis.atemschutzueberwachung.widget.trupp.TruppenStatusView;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Anwendungscontroller;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Ausrückung;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.IInteractionRequestListener;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.InteractionRequest;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Luftstand;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Rückkehr;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Rückzugsantritt;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Stammdaten;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Truppenstatus;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Zeitpunkt;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Zielerfüllung;

import static android.os.Looper.getMainLooper;
import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.BDLKEY_ATTACK;

public class TruppDetailFragment extends Fragment {

    public static final String TAG = "fragment_trupp";

    private Atemschutzeinsatz attack;

    private TruppenStatusView statusView;

    private Anwendungscontroller controller;

    private ValueAnimator blinkingAnimation;
    private ValueAnimator endAnimation;

    private final IInteractionRequestListener backgroundHighlightListener = new IInteractionRequestListener() {
        @Override
        public void onInteractionRequested(HashMap<Atemschutzeinsatz, List<InteractionRequest>> map) {
            new Handler(getMainLooper()).post(() -> {
                long nowMinusThirty = System.currentTimeMillis() - 30000L;
                if (Atemschutz.lastInteraction >= nowMinusThirty || map.values().size() == 0) {
                    if (blinkingAnimation.isRunning())
                        blinkingAnimation.end();
                    endAnimation.start();
                } else {
                    if (!blinkingAnimation.isRunning())
                        blinkingAnimation.start();
                }
            });
        }
    };

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            controller = ((OperationService.OperationServiceBinder) service).getOperationService().getController();
            controller.registerInteractionRequestListener(backgroundHighlightListener);
            updateState(statusView, controller, false);
            Truppenstatus state = controller.getStateOf(attack.getTroup());
            actionView.disableInvalidActions(state);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            controller.unregisterInteractionRequestListener(backgroundHighlightListener);
        }
    };

    private final OnBackPressedCallback backpressCallback = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            applyChanges();
            getParentFragment().getChildFragmentManager()
                    .popBackStack();
        }
    };

    private TruppLuftstandZählerView airView;
    private TruppAktionView actionView;
    private TruppAktionView.Callback actionsCallback = new TruppAktionView.Callback() {
        @Override
        public void setAusrückung(@Nullable Date d) {
            if (null == d) {
                removeTimeStampOfClass(Ausrückung.class);
            } else controller.setAttacking(attack);
            updateState(statusView, controller, true);
        }

        @Override
        public void setSuccess(@Nullable Date d) {
            if (null == d) {
                removeTimeStampOfClass(Zielerfüllung.class);
            } else controller.setSuccess(attack);
            updateState(statusView, controller, true);
        }

        @Override
        public void setRückzug(@Nullable Date d) {
            if (null == d) {
                removeTimeStampOfClass(Rückzugsantritt.class);
            } else controller.setRetreating(attack);
            updateState(statusView, controller, true);
        }

        @Override
        public void setZurück(@Nullable Date d) {
            if (null == d) {
                removeTimeStampOfClass(Rückkehr.class);
            } else controller.setRetreated(attack);
            updateState(statusView, controller, true);
        }

        private void removeTimeStampOfClass(Class<?> aClass) {
            for ( Zeitpunkt ts : attack.getAchievements() ) {
                if (ts.getClass() == aClass) {
                    attack.getAchievements().remove(ts);
                }
            }
        }
    };

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        if (args == null) throw new IllegalStateException(String.format("%s requires a Atemschutzeinsatz object.", TruppDetailFragment.class));
        attack = (Atemschutzeinsatz) args.getSerializable(BDLKEY_ATTACK);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getOnBackPressedDispatcher()
                .addCallback(backpressCallback);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trupp_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ConstraintLayout rootLayout = view.findViewById(R.id.trupp_detail_root);

        blinkingAnimation = ((MainActivity) getActivity()).getBlinkingAnimation(false);
        endAnimation = ((MainActivity) getActivity()).getBlinkingAnimation(true);
        ValueAnimator.AnimatorUpdateListener animUpdate = animator -> {
            if (rootLayout != null)
                rootLayout.setBackgroundColor((int) animator.getAnimatedValue());
        };
        endAnimation.addUpdateListener(animUpdate);
        blinkingAnimation.addUpdateListener(animUpdate);
        final Stammdaten details = attack.getTroup().getTroupDetails();

        ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_desc))
                .getEditText().setText(details.getTeamDescription());
        ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_fuehrer))
                .getEditText().setText(details.getTeamLeader());
        ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_mann_1))
                .getEditText().setText(details.getTeamMember_1());
        ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_mann_2))
                .getEditText().setText(details.getTeamMember_2());
        ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_funk))
                .getEditText().setText(details.getRadioChannel());

        statusView = ((TruppenStatusView) view.findViewById(R.id.trupp_detail_status));

        airView = ((TruppLuftstandZählerView) view.findViewById(R.id.trupp_detail_luftstand));
        airView.setData(attack);

        actionView = ((TruppAktionView) view.findViewById(R.id.trupp_detail_action_container));
        actionView.setData(attack);

        view.findViewById(R.id.view_luftstand_remove)
                .setOnClickListener((v) -> {
                    Luftstand last = null;
                    for (Zeitpunkt p : attack.getAchievements()) {
                        if (p instanceof Luftstand)
                            last = (Luftstand) p;
                    }
                    if (null != last) {
                        attack.getAchievements().remove(last);
                        onAirUpdated();
                        if (controller != null) {
                            controller.onDataUpdated();
                        }
                    }
                });

        view.findViewById(R.id.trupp_detail_luftstandabgleich)
                .setOnClickListener(v -> {
                    Bundle bdl = new Bundle();
                    bdl.putSerializable(BDLKEY_ATTACK, attack);
                    LuftstandAbgleichFragment luftstandAbgleichFragment = new LuftstandAbgleichFragment();
                    luftstandAbgleichFragment.setArguments(bdl);
                    getParentFragment()
                            .getChildFragmentManager().beginTransaction()
                            .add(R.id.einsatz_uebersicht_root, luftstandAbgleichFragment, LuftstandAbgleichFragment.TAG)
                            .addToBackStack(null)
                            .commit();
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        final Intent serviceIntent = new Intent(getContext(), OperationService.class);
        getContext().bindService(serviceIntent, serviceConnection, Context.BIND_IMPORTANT);
        this.backpressCallback.setEnabled(true);
        actionView.attachCallbacks(actionsCallback);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.backpressCallback.setEnabled(false);
        actionView.detachCallbacks();
    }

    @Override
    public void onStop() {
        super.onStop();
        getContext().unbindService(serviceConnection);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.backpressCallback.remove();
    }

    private void updateState(TruppenStatusView statusView, Anwendungscontroller controller, boolean userFeedback) {
        Truppenstatus state = controller.getStateOf(attack.getTroup());
        TruppDetailViewStatus viewState = TruppDetailViewStatus.ERSTELLT;
        boolean show = true;
        switch (state) {
            case NICHT_EINSATZBEREIT:
                show = false;
            case EINSATZBEREIT:
                viewState = TruppDetailViewStatus.ERSTELLT;
                break;
            case AUSGERÜCKT:
                viewState = TruppDetailViewStatus.AUSGERÜCKT;
                break;
            case ERFOLGREICH:
                viewState = TruppDetailViewStatus.AUSGERÜCKT;
                break;
            case RÜCKKEHREND:
                viewState = TruppDetailViewStatus.RÜCKZUG;
                break;
            case RUHEPAUSE:
                viewState = TruppDetailViewStatus.RÜCKKEHR;
                break;
            case RETTUNGSEINSATZ:
                viewState = TruppDetailViewStatus.RETTUNG;
                break;
        }
        statusView.setInternalState(viewState);
        actionView.disableInvalidActions(state);

        if (!show || !userFeedback) return;
        String msg = String.format("Neuer Truppenstatus: %s", state.name());
        ((MainActivity) getActivity()).showUserFeedback(msg);
    }

    private void applyChanges(){
        View view = requireView();
        final Stammdaten details = attack.getTroup().getTroupDetails();

        final Stammdaten throwaway = new Stammdaten();
        String name, tl, tm1, tm2, radio;
        name = details.getTeamDescription();
        tl = details.getTeamLeader();
        tm1 = details.getTeamMember_1();
        tm2 = details.getTeamMember_2();
        radio = details.getRadioChannel();
        throwaway.setTeamDescription(name);
        throwaway.setTeamLeader(tl);
        throwaway.setTeamMember_1(tm1);
        throwaway.setTeamMember_2(tm2);
        throwaway.setRadioChannel(radio);

        details.setTeamDescription(
                ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_desc))
                        .getEditText().getText().toString()
        );
        details.setTeamLeader(
                ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_fuehrer))
                        .getEditText().getText().toString()
        );
        details.setTeamMember_1(
                ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_mann_1))
                        .getEditText().getText().toString()
        );
        details.setTeamMember_2(
                ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_mann_2))
                        .getEditText().getText().toString()
        );
        details.setRadioChannel(
                ((TextInputLayout) view.findViewById(R.id.trupp_detail_trupp_funk))
                        .getEditText().getText().toString()
        );

        if (throwaway.equals(details)) return;

        controller.onDataUpdated();
        String msg = String.format("Atemschutztrupp %s wurde aktualisiert.", details.getTeamDescription());
        ((MainActivity) getActivity()).showUserFeedback(msg);

    }

    public void onAirUpdated() {
        airView.setData(attack);
        actionView.setData(attack);
        ((MainActivity) getActivity()).showUserFeedback("Luftstandsmenge aktualisiert!");
        if (null != controller) {
            Truppenstatus state = controller.getStateOf(attack.getTroup());
            actionView.disableInvalidActions(state);
        }
    }
}
