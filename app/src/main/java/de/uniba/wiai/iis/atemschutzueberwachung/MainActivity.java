/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iis.atemschutzueberwachung;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;
import java.util.List;

import de.uniba.wiai.iis.atemschutzueberwachung.fragment.einsatz.EinsatzUebersichtFragment;
import de.uniba.wiai.iis.atemschutzueberwachung.widget.InteractionAwareLayout;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Anwendungscontroller;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.Atemschutzeinsatz;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.IInteractionRequestListener;
import de.uniba.wiai.iss.atemschutzueberwachung.domain.InteractionRequest;

import static de.uniba.wiai.iis.atemschutzueberwachung.Atemschutz.POWER_MANAGER_WAKE_LOCK_REASON;

public class MainActivity extends AppCompatActivity {

    private PowerManager.WakeLock wakeLock;

    private InteractionAwareLayout interactionAwareLayout;

    private Anwendungscontroller controller;

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            OperationService operationService = ((OperationService.OperationServiceBinder) service).getOperationService();
            controller = operationService.getController();
            controller.registerInteractionRequestListener(eventListener);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private final InteractionAwareLayout.Listener noteInteractionListener = () -> Atemschutz.lastInteraction = System.currentTimeMillis();

    private final IInteractionRequestListener eventListener = new IInteractionRequestListener() {
        @Override
        public void onInteractionRequested(HashMap<Atemschutzeinsatz, List<InteractionRequest>> map) {
            new Handler(getMainLooper()).post(() -> {
                if (0 == map.values().size()) {
                    if (wakeLock.isHeld())
                        wakeLock.release();
                } else {
                    if (!wakeLock.isHeld())
                        wakeLock.acquire(605000L);
                    long lastInteractionCheck = System.currentTimeMillis() - 30000L;
                    if (lastInteractionCheck < Atemschutz.lastInteraction)
                        return;
                    if (!isPlaying) {
                        soundPool.play(soundId, 1, 1, 0, 1, 1);
                        isPlaying = true;
                        new Handler().postDelayed(() -> isPlaying = false, 3500L);
                    }
                }
            });
        }
    };

    private final SoundPool soundPool = new SoundPool(1, AudioManager.STREAM_ALARM , 0);
    private int soundId = 0;
    private boolean isPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        soundId = soundPool.load(this, R.raw.alarm, 1);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        int cpuAndAwake = 0; // flags:
        /* enable CPU scheduling */
        cpuAndAwake |= PowerManager.PARTIAL_WAKE_LOCK;
        /* wake up screen */
        cpuAndAwake |= PowerManager.ACQUIRE_CAUSES_WAKEUP;
        wakeLock = powerManager.newWakeLock(cpuAndAwake, POWER_MANAGER_WAKE_LOCK_REASON);

        setContentView(R.layout.activity_main);

        interactionAwareLayout = findViewById(R.id.activity_frag_container);
        interactionAwareLayout.setCallback(noteInteractionListener);

        final Intent serviceIntent = new Intent(this.getApplicationContext(), OperationService.class);

        if (null == savedInstanceState) {
            startService(serviceIntent);

            Dialog info = new BottomSheetDialog(this);
            info.setTitle("Benutzeranleitung");
            info.setContentView(R.layout.dialog_usage_info);
            info.setCancelable(true);
            info.show();
            BottomSheetBehavior.from(info.findViewById(R.id.design_bottom_sheet)).setState(BottomSheetBehavior.STATE_EXPANDED);

            Fragment einsatzUebersichtFragment = new EinsatzUebersichtFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.activity_frag_container, einsatzUebersichtFragment, EinsatzUebersichtFragment.TAG)
                    .commit();
        } else { /* restored by android */ }

        bindService(serviceIntent, serviceConnection, BIND_IMPORTANT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        controller.unregisterInteractionRequestListener(eventListener);
        unbindService(serviceConnection);

        if (wakeLock.isHeld())
            wakeLock.release();
    }

    public ValueAnimator getBlinkingAnimation(boolean end) {
        int urgent = getResources().getColor(R.color.urgent);
        int normal = getResources().getColor(R.color.white);
        ValueAnimator anim = ValueAnimator.ofObject(new ArgbEvaluator(), end? normal : urgent, normal);
        anim.setDuration(1000L);
        anim.setRepeatCount(end? 0 : ValueAnimator.INFINITE);
        anim.setRepeatMode(ValueAnimator.REVERSE);
        return anim;
    }

    public void showUserFeedback(String msg) {
        Snackbar.make(interactionAwareLayout, msg, Snackbar.LENGTH_SHORT).show();
    }
}
