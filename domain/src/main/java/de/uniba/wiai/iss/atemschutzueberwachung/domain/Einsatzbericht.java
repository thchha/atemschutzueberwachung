/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iss.atemschutzueberwachung.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Einsatzbericht implements Serializable {

    private final List<Atemschutzeinsatz> attacks;
    private final long start;

    public Einsatzbericht(long start, List<Atemschutzeinsatz> attacks) {
        super();
        this.start = start;
        this.attacks = attacks;
    }

    public List<Atemschutznachweis> getTroupReports() throws AnwendungslogikException {
        HashMap<String, Atemschutznachweis> eachPerson = new HashMap<>();

        for (Atemschutzeinsatz e : attacks) {
            Stammdaten details = e.getTroup().getTroupDetails();
            String activity = e.getTroup().getActivity();
            String equipment = e.getRespiratorySystem();
            List<Zeitpunkt> ts = e.getAchievements();

            Ausrückung start = null;
            Rückkehr end = null;
            for (Zeitpunkt z : ts) {
                if (z instanceof Ausrückung) {
                    start = (Ausrückung) z;
                }
                if (z instanceof Rückkehr) {
                    end = (Rückkehr) z;
                }
            }

            if (null == start) { // not of interest anymore
                continue;
            }

            if (null == end) {
                throw new AnwendungslogikException(AnwendungslogikException.Reason.INVALID_ATTACK_STATE);
            }

            String tl, tm1, tm2;
            tl = details.getTeamLeader();
            tm1 = details.getTeamMember_1();
            tm2 = details.getTeamMember_2();

            if (!eachPerson.containsKey(tl)){
               eachPerson.put(tl, new Atemschutznachweis(tl));
            }
            Atemschutznachweis leaderReport = eachPerson.get(tl);
            leaderReport.add(start, end, activity, equipment);

            if (null != tm1 && !tm1.isEmpty()) {
                if (!eachPerson.containsKey(tm1)){
                    eachPerson.put(tm1, new Atemschutznachweis(tm1));
                }
                Atemschutznachweis tm1Report = eachPerson.get(tm1);
                tm1Report.add(start, end, activity, equipment);
            }

            if (null != tm2 && !tm2.isEmpty()) {
                if (!eachPerson.containsKey(tm2)){
                    eachPerson.put(tm2, new Atemschutznachweis(tm2));
                }
                Atemschutznachweis tm2Report = eachPerson.get(tm2);
                tm2Report.add(start, end, activity, equipment);
            }
        }

        return new LinkedList<>(eachPerson.values());
    }

    public String getContentAsText(String reasoning, String location, String personInCharge) throws AnwendungslogikException {
        List<Atemschutznachweis> all = getTroupReports();
        Date now = new Date();
        long diff = System.currentTimeMillis() - start / 1000;
        String duration = String.format("%d Stunden & %02d min",
                TimeUnit.MILLISECONDS.toHours(diff),
                TimeUnit.MILLISECONDS.toMinutes(diff % 60000));
        SimpleDateFormat sdf = new SimpleDateFormat("dd.mm.yyyy HH:MM");
        String content = String.format("Einsatzbericht vom %s\n\n", sdf.format(new Date(start)));
        content += String.format("Einsatzziel / Ursache: \t %s", reasoning);
        content += String.format("Einsatzort: \t %s", location);
        content += String.format("Verantwortlicher für die Atemschutzüberwachung: \t %s", personInCharge);
        content += String.format("Einsatzende: \t %s", sdf.format(now));
        content += String.format("Einsatzdauer: \t %s", duration);
        content += "\n===============================================\n\n";

        for (Atemschutznachweis rep: all) {
            content += String.format("Atemschutznachweis für Person: \t %s\n", rep.getName());
            content += String.format("Gesamte Einsatzdauer:\t %s\n", rep.getCumulativeAttackDuration());
            for (Atemschutznachweis.Einsatz e : rep.getOwnedAttacks()){
                content += String.format("Dauer der Tätigkeit \"%s\" unter Zuhilfename von \"%s\":\t %s\n", e.getActivity(), e.getEquipment(), e.getDuration());
            }
            content += "\n- - - - - - - - - - - - - - - - - - - - - - - -\n";
        }

        return content;
    }
}
