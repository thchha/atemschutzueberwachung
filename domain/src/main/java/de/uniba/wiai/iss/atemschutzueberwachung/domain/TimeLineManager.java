/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iss.atemschutzueberwachung.domain;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

class TimeLineManager {

    /**
     * Only check once per second for work
     */
    private static final long RESPONSIVENESS = 996L;

    /**
     * To display actions which will occur in a few,
     * use this variable to adapt the critical time to
     * issue an event.
     *
     * 30 seconds is a magical number
     */
    private static final long IMMEDIATE_THRESHOLD = 30000L;

    /* convenience */
    private static final long TEN_MINUTES = 600000L;
    private static final long THIRTY_MINUTES = 1800000L;

    private final Anwendungscontroller controller;
    private final ScheduledExecutorService threadExecutor;

    private boolean stopped = false;

    List<IInteractionRequestListener> listeners = new LinkedList<>();

    public TimeLineManager(Anwendungscontroller controller) {
        super();
        this.controller = controller;
        threadExecutor = Executors.newScheduledThreadPool(1);
    }

    private final Runnable eventBusHandler = new Runnable() {
        @Override
        public void run() {

            // TODO: synchronization - unsafe access -
            List<Atemschutzeinsatz> attacks = new LinkedList<>(controller.attacks);
            HashMap<Atemschutzeinsatz, List<InteractionRequest>> map = new HashMap<>();

            for (Atemschutzeinsatz e : attacks) {
                List<Luftstand> air = new LinkedList<>();
                Ausrückung attacking = null;
                Rückzugsantritt retrieving = null;
                Rückkehr backed = null;
                List<InteractionRequest> items = new LinkedList<>();

                long now = System.currentTimeMillis();

                for (Zeitpunkt p : e.getAchievements()) {
                    if (p instanceof Luftstand) {
                        air.add((Luftstand) p);
                    }
                    if (p instanceof Ausrückung) {
                        attacking = (Ausrückung) p;
                    }
                    if (p instanceof Rückzugsantritt) {
                        retrieving = (Rückzugsantritt) p;
                    }
                    if (p instanceof Rückkehr){
                        backed = (Rückkehr) p;
                    }
                }

                InteractionRequest event = null;
                long exact;

                if (null != backed) {
                    exact = backed.timeStamp + THIRTY_MINUTES;
                    if ( (now - IMMEDIATE_THRESHOLD) >= exact ) {
                        event = InteractionRequest.RUHEZEITENENDE;
                        event.setEstimatedTimeStamp(exact);
                        items.add(event);
                        map.put(e, items);
                        // TODO: needs to mark attack as finished, once the user noted the action.
                    }
                    continue;
                }

                if (null != retrieving) {
                    long diff = retrieving.timeStamp - attacking.timeStamp;
                    exact = retrieving.timeStamp + diff;
                    if (exact <= ( now - IMMEDIATE_THRESHOLD) ) {
                        event = InteractionRequest.RÜCKKEHRERWARTUNG;
                        event.setEstimatedTimeStamp(exact);
                        items.add(event);
                        map.put(e, items);
                        continue;
                    }
                }

                if (null != attacking) {

                    long remainingTime = e.getRemainingAttackTime();
                    if (air.size() > 1 &&
                            remainingTime <= IMMEDIATE_THRESHOLD) {
                        exact = now + remainingTime;
                        event = InteractionRequest.RÜCKZUGSAUFFORDERUNG;
                        event.setEstimatedTimeStamp(exact);
                        items.add(event);
                        map.put(e, items);
                    }

                    Luftstand last = air.get(air.size() -1 );
                    exact = last.timeStamp + TEN_MINUTES;
                    if (exact <= ( now + IMMEDIATE_THRESHOLD) ) {
                        event = InteractionRequest.LUFTSTANDSABFRAGE;
                        event.setEstimatedTimeStamp(exact);
                        items.add(event);
                        map.put(e, items);
                    }
                }
            }

            emitInteractionRequest(map);

            if (!isStopped()) {
                // save energy
                threadExecutor.schedule(this, RESPONSIVENESS, TimeUnit.MILLISECONDS);
            }
        }
    };

    private void emitInteractionRequest(HashMap<Atemschutzeinsatz, List<InteractionRequest>> map) {
        if (isStopped()) return;
        for (IInteractionRequestListener listener : listeners) {
            listener.onInteractionRequested(map);
        }
    }

    private synchronized boolean isStopped() {
        return stopped;
    }

    public synchronized void stopMonitoring() {
        stopped = true;

        try {
            threadExecutor.awaitTermination(500L, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            try {
                threadExecutor.shutdownNow();
            } catch (SecurityException s) {
                // swallow
            }
        }
    }

    public void startMonitoring() {
        threadExecutor.submit(eventBusHandler);
    }
}
