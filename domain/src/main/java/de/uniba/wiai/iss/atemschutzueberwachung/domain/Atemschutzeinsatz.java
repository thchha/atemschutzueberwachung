/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iss.atemschutzueberwachung.domain;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Atemschutzeinsatz implements Serializable {

    private final Atemschutztrupp troup;

    private String respiratorySystem = "";
    private final List<Zeitpunkt> achievements = new LinkedList<>();

    public Atemschutzeinsatz(Atemschutztrupp troup) {
        super();
        this.troup = troup;
    }

    public Atemschutztrupp getTroup() {
        return troup;
    }

    public List<Zeitpunkt> getAchievements() {
        return achievements;
    }

    public String getRespiratorySystem() {
        return respiratorySystem;
    }

    public void setRespiratorySystem(String respiratorySystem) {
        this.respiratorySystem = respiratorySystem;
    }

    public Truppenstatus getState() {
        Truppenstatus state = Truppenstatus.NICHT_EINSATZBEREIT;
        boolean ready = false;
        for (Zeitpunkt z : getAchievements()) {
            if (z instanceof Luftstand) {
                ready = true;
            }
            if (z instanceof Ausrückung) {
                state = Truppenstatus.AUSGERÜCKT;
                String rettung = Tätigkeit.SICHERHEITSTRUPP.name();
                if (getTroup().getActivity().equals(rettung)){
                    state = Truppenstatus.RETTUNGSEINSATZ;
                }
            }
            if (z instanceof Zielerfüllung) {
                state = Truppenstatus.ERFOLGREICH;
            }
            if (z instanceof Rückzugsantritt) {
                state = Truppenstatus.RÜCKKEHREND;
            }
            if (z instanceof Rückkehr) {
                state = Truppenstatus.RUHEPAUSE;
            }
        }

        return Truppenstatus.NICHT_EINSATZBEREIT == state && ready ? Truppenstatus.EINSATZBEREIT : state;
    }

    public long getRemainingAttackTime() {
        Luftstand first = null;
        List<Luftstand> air = new LinkedList<>();
        for (Zeitpunkt p : achievements) {
            if (p instanceof Luftstand) {
                air.add((Luftstand) p);
            }
        }

        if (air.size() > 1) {
            first = air.remove(0);
            long maxConsume = 0L;
            long minimal = (long) (first.getAvailableAir() * 66);

            long lastAir = (long) (first.getAvailableAir() * 100);
            long lastTS = first.timeStamp;

            for (Luftstand l : air) {
                long consumed = (long) ((l.timeStamp - lastTS) / (lastAir - (l.getAvailableAir() * 100) ));
                if (consumed > maxConsume)
                    maxConsume = consumed;

                lastTS = l.timeStamp;
                lastAir = (long) (l.getAvailableAir() * 100);
            }

            return (long) (maxConsume * ((100 * air.get(air.size() -1 ).getAvailableAir() ) - minimal));
        } else {
            return 0L;
        }
    }
}
