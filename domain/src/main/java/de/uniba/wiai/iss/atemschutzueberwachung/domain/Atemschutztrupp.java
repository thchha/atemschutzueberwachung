/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iss.atemschutzueberwachung.domain;

import java.io.Serializable;

public class Atemschutztrupp implements Serializable {

    private final Stammdaten details;
    private String activity = "";

    public Atemschutztrupp(Stammdaten details) {
        super();
        this.details = details;
    }

    public String getActivity() {
        return activity;
    }

    public Stammdaten getTroupDetails() {
        return details;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (null == o) return false;
        if (getClass() != o.getClass()) return false;
        return ((Atemschutztrupp) o).details.equals(this.details);
    }

    @Override
    public int hashCode() {
        return details.hashCode() * activity.hashCode();
    }
}
