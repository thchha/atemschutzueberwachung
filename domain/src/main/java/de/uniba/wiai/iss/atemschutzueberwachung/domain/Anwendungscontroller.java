/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iss.atemschutzueberwachung.domain;

import java.util.LinkedList;
import java.util.List;

public class Anwendungscontroller {

    private final TimeLineManager eventEmitter = new TimeLineManager(this);

    final List<Atemschutzeinsatz> attacks = new LinkedList<>();
    private final List<IAnwendungsdatenObserver> observers = new LinkedList<>();
    private long start;

    public void onStop() {
        eventEmitter.stopMonitoring();
    }

    public Truppenstatus getStateOf(Atemschutztrupp troup) {
        List<Atemschutzeinsatz> solo = new LinkedList<>();
        for (Atemschutzeinsatz a: attacks) {
            if (troup.equals(a.getTroup())) {
               solo.add(a);
            }
        }

        // filter out old ones
        for (Atemschutzeinsatz a: solo) {
            for (Zeitpunkt z : a.getAchievements()){
                if (z instanceof Rückkehr) {
                    if (z.timeStamp + (30*60*1000) < System.currentTimeMillis())
                        solo.remove(a);
                }
            }
        }

        if (solo.size() == 0)
            throw new IllegalArgumentException("used Atemschutztrupp is not known.");

        if (solo.size() > 1)
            throw new IllegalStateException("duboius troups exist");

        return solo.get(0).getState();
    }

    public Atemschutzeinsatz registerTroup(Atemschutztrupp troup, boolean forceDuplicateNameInAction) throws AnwendungslogikException {
        if (null == troup)
            throw new IllegalArgumentException("No Atemschutztrupp provided");

        Stammdaten details = troup.getTroupDetails();

        String tl = details.getTeamLeader();
        if (tl == null || tl.isEmpty()) {
            throw new AnwendungslogikException(AnwendungslogikException.Reason.LEADER_MISSING);
        }

        String tm1 = details.getTeamMember_1();
        String tm2 = details.getTeamMember_2();
        if ((tm1 == null || tm1.isEmpty()) && (tm2 == null || tm2.isEmpty())) {
            throw new AnwendungslogikException(AnwendungslogikException.Reason.TEAMMEMBERS_MISSING);
        }

        for (Atemschutzeinsatz a: attacks) {
            Atemschutztrupp trupp = a.getTroup();
            Truppenstatus state = getStateOf(trupp);

            // check if any person already appears in a ongoing attack
            if ( !forceDuplicateNameInAction &&
                    (state != Truppenstatus.EINSATZBEREIT && state != Truppenstatus.RUHEPAUSE && state != Truppenstatus.NICHT_EINSATZBEREIT)) {
                Stammdaten det = trupp.getTroupDetails();
                String[] names = new String[] { det.getTeamLeader(), det.getTeamMember_1(), det.getTeamMember_2() };
                for (String n : names) {
                    if (n.equals(tm1) || n.equals(tm2))
                        throw new AnwendungslogikException(AnwendungslogikException.Reason.TEAMMEMBER_ALREADY_INACTION);
                }
            }

            if (troup.equals(trupp)) { // is the exact same group already declared?

                if (state != Truppenstatus.RUHEPAUSE && state != Truppenstatus.EINSATZBEREIT && state != Truppenstatus.NICHT_EINSATZBEREIT) {
                    throw new AnwendungslogikException(AnwendungslogikException.Reason.OLD_STATE_APPEARS_INVALID);
                }
            }
        }

        Atemschutzeinsatz attack = new Atemschutzeinsatz(troup);
        attacks.add(attack);

        internalNotify();
        return attack;
    }

    public void updateAvailableAir(Atemschutzeinsatz attack, double value) throws AnwendungslogikException {
        /*
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!!!!!!!!!!!!! MAGIC NUMBER !!!!!!!!!!!!!!!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            what pressure is required as a minimum?
            Also, it does not handle translation between bar / psi
         */
        if (value < 0.5) {
           throw new AnwendungslogikException(AnwendungslogikException.Reason.LUFTSTANDSMENGE_UNGÜLTIG);
        }

        List<Luftstand> air = new LinkedList<>();
        for (Zeitpunkt z : attack.getAchievements()){
            if (z instanceof Luftstand){
                air.add((Luftstand) z);
            }
        }

        if (air.size() > 0){
            Luftstand last = air.get(air.size() -1);
            if (last.getAvailableAir() <= value) {
                throw new AnwendungslogikException(AnwendungslogikException.Reason.LUFTSTANDSMENGE_UNGÜLTIG);
            }
        }

        attack.getAchievements().add(new Luftstand(value));

        internalNotify();
    }

    public void setAttacking(Atemschutzeinsatz attack) {
        for (Zeitpunkt z : attack.getAchievements()) {
            if (z instanceof Ausrückung) {
                throw new IllegalStateException("Atemschutztrupp already in action. Domain error");
            }
        }

        attack.getAchievements().add(new Ausrückung());

        internalNotify();
    }

    public void setRetreating(Atemschutzeinsatz attack) {
        for (Zeitpunkt z : attack.getAchievements()) {
            if (z instanceof Rückzugsantritt) {
                throw new IllegalStateException("Atemschutztrupp already retreating. Domain error");
            }
        }

        attack.getAchievements().add(new Rückzugsantritt());

        internalNotify();
    }

    public void setSuccess(Atemschutzeinsatz attack) {
        for (Zeitpunkt z : attack.getAchievements()) {
            if (z instanceof Zielerfüllung) {
                throw new IllegalStateException("Atemschutztrupp already succeeded. Domain error");
            }
        }

        attack.getAchievements().add(new Zielerfüllung());

        internalNotify();
    }

    public void setRetreated(Atemschutzeinsatz attack) {
        for (Zeitpunkt z : attack.getAchievements()) {
            if (z instanceof Rückkehr) {
                throw new IllegalStateException("Atemschutztrupp already returned. Domain error");
            }
        }

        attack.getAchievements().add(new Rückkehr());

        internalNotify();
    }

    public void onStart() {
        start = System.currentTimeMillis();
        eventEmitter.startMonitoring();
    }

    public Einsatzbericht produceReport() {
        return new Einsatzbericht(start, attacks);
    }

    public void registerObserver(IAnwendungsdatenObserver observer) {
        observers.add(observer);
        internalNotify();
    }

    public void unregisterObserver(IAnwendungsdatenObserver observer) {
        observers.remove(observer);
    }

    public void registerInteractionRequestListener(IInteractionRequestListener listener) {
        eventEmitter.listeners.add(listener);
    }

    public void unregisterInteractionRequestListener(IInteractionRequestListener listener) {
        eventEmitter.listeners.remove(listener);
    }

    public void onDataUpdated(){
        internalNotify();
    }

    private void internalNotify() {
        // TODO: Verify integrity..
        for (IAnwendungsdatenObserver ob : observers) {
            ob.onDataUpdated(attacks);
        }
    }
}
