/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iss.atemschutzueberwachung.domain;

public class AnwendungslogikException extends Exception {

    public final Reason reason;

    public AnwendungslogikException(Reason r) {
        super(r.msg);
        this.reason = r;
    }

    public enum Reason {
        LEADER_MISSING("no teamleader provided"),
        TEAMMEMBERS_MISSING("no teammembers provided"),
        OLD_STATE_APPEARS_INVALID("the same team appears already to be in action."),
        LUFTSTANDSMENGE_UNGÜLTIG("the value appears to be invalid"),
        INVALID_ATTACK_STATE("There are ongoing attacks"),
        TEAMMEMBER_ALREADY_INACTION("The teammember is already in action, how to proceed? ");

        private final String msg;

        Reason(String msg){
            this.msg = msg;
        }
    }
}
