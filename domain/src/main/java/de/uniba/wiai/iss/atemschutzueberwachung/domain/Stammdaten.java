/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iss.atemschutzueberwachung.domain;

import java.io.Serializable;
import java.util.Objects;

public class Stammdaten implements Serializable {
    private String teamDescription;
    private String teamLeader;
    private String teamMember_1;
    private String teamMember_2;
    private String radioChannel;


    public String getTeamDescription() {
        return teamDescription;
    }

    public String getTeamLeader() {
        return teamLeader;
    }

    public String getTeamMember_1() {
        return teamMember_1;
    }

    public String getTeamMember_2() {
        return teamMember_2;
    }

    public String getRadioChannel() {
        return radioChannel;
    }

    public void setTeamDescription(String teamDescription) {
        this.teamDescription = teamDescription;
    }

    public void setTeamLeader(String teamLeader) {
        this.teamLeader = teamLeader;
    }

    public void setTeamMember_1(String teamMember_1) {
        this.teamMember_1 = teamMember_1;
    }

    public void setTeamMember_2(String teamMember_2) {
        this.teamMember_2 = teamMember_2;
    }

    public void setRadioChannel(String radioChannel) {
        this.radioChannel = radioChannel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stammdaten that = (Stammdaten) o;
        return teamLeader.equals(that.teamLeader) &&
                teamMember_1.equals(that.teamMember_1) &&
                teamMember_2.equals(that.teamMember_2) &&
                teamDescription.equals(that.teamDescription) &&
                radioChannel.equals(that.radioChannel);
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash *= teamDescription.hashCode();
        hash *= teamLeader.hashCode();
        hash *= teamMember_1.hashCode();
        hash *= teamMember_2.hashCode();
        hash *= radioChannel.hashCode();
        hash = 37 * hash + 17;
        return hash;
    }
}

