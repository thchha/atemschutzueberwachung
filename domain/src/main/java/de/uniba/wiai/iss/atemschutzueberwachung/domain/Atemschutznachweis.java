/*
    <Atemschutzüberwachung>
    Copyright (C) 2021 Thomas Hage

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/
package de.uniba.wiai.iss.atemschutzueberwachung.domain;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Atemschutznachweis {

    private final String name;

    private List<Einsatz> attacks = new LinkedList<>();

    public Atemschutznachweis(String person) {
        super();
        name = person;
    }

    public List<Einsatz> getOwnedAttacks() {
        return attacks;
    }

    public String getCumulativeAttackDuration() {
        long duration = 0L;
        for (Einsatz e : attacks) {
            duration += e.end.timeStamp - e.start.timeStamp;
        }
        return String.format("%d Stunden & %02d min",
                TimeUnit.MILLISECONDS.toHours(duration),
                TimeUnit.MILLISECONDS.toMinutes(duration % 60000));
    }

    public void add(Ausrückung start, Rückkehr end, String activity, String equipment) {
        attacks.add(new Einsatz(start, end, activity, equipment));
    }

    public String getName() {
        return name;
    }

    public class Einsatz {
        private final Ausrückung start;
        private final Rückkehr end;
        private final String activity;
        private final String equipment;

        public Einsatz(Ausrückung start, Rückkehr end, String activity, String equipment) {
            this.start = start;
            this.end = end;
            this.activity = activity;
            this.equipment = equipment;
        }

        public String getDuration() {
            long diff = end.timeStamp - start.timeStamp;
            return String.format("%d Std. & %02d Min.",
                    TimeUnit.MILLISECONDS.toHours(diff),
                    TimeUnit.MILLISECONDS.toMinutes(diff));
        }

        public String getActivity() {
            return activity;
        }

        public String getEquipment() {
            return equipment;
        }
    }
}
